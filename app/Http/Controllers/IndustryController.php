<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\industry;
use Illuminate\Support\Facades\Validator;


class IndustryController extends Controller {


    public function getIndusrty(Request $request){
        try {

            $crawler =   \Goutte::request('GET', 'http://www.mycorporateinfo.com/');
            $crawler->filter('ul > li > a')->each(function ($node) { // get all anchor tage data
                $href = $node->attr('href');
                $text = $node->text();
                if(strpos($href,"industry") ){
                    $section = explode('/',$href)[3]; // section value
                    $industry = industry::firstOrNew(array('name' => $text)); // if industy exits then update other wise insert
                    $industry->name =  $text;
                    $industry->section = $section;
                    $industry->save();
                } 
            });
            
            echo "all Industry updated!";
            
        } catch (\Exception $ex) {
            echo  'error occured '.$e->getMessage().' on line '.$e->getLine(). ' file '.$e->getFile();
        }
    }

    public function index(Request $request) {
        $industry = industry::all();
        return view('industry', ['industries' => $industry]);
    }



    public function getSectionIndusrty(Request $request){
        try {
            $crawler =   \Goutte::request('GET', 'http://www.mycorporateinfo.com/industry/section/C/page/2');
            $text = [];
            $crawler->filter('.pagination li a')->each(function ($node) use (&$text) {
            $text[] = $node->text();
            return $text;
            });

            $pagecont = ((int) array_key_last ( $text ) )-1 ;
            $lastpage =  $text[$pagecont];
            

            $table = $crawler->filter('table')->filter('tr')->each(function ($tr, $i) {
                return $tr->filter('td')->each(function ($td, $i) {
                    return trim($td->text());
                });
            });


            
            echo "all Industry updated!";
            
        } catch (\Exception $e) {
            echo  'error occured '.$e->getMessage().' on line '.$e->getLine(). ' file '.$e->getFile();
        }
    }



}